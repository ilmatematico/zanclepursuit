#pragma once
#include <Gl/glew.h>
#include "GamePhase.h"
class GameMenu : public GamePhase
{
public:
	GameMenu();
	~GameMenu();

	virtual void render();
	virtual void reshape(int w, int h);
	virtual void keyboard(unsigned char key, int x, int y);
	virtual void keyboardup(unsigned char key, int x, int y);
	virtual void arrowKeys(int key, int x, int y);
	virtual void arrowKeysUp(int key, int x, int y);
	virtual void idle();
	virtual void startgame();

	void accendiIniziaPartita();
	void accendiOpzioni();
	void accendiEsci();


private:
	int pos = 0;
	int red[3];
	int green[3];
	int blu[3];
	bool istruzioni=false;
	char* mainScenePath = "models\\scena.obj";
	
};
