#pragma once

#include <GL/glew.h>
#include "stdfx.h"
#include "GameMenu.h"
#include "GameScene.h"
#include <map>
#include <SFML/System.hpp>
#include <SFML/Audio.hpp>


#define aisgl_min(x,y) (x<y?x:y)
#define aisgl_max(x,y) (y>x?y:x)
#define TRUE                1
#define FALSE               0


extern GameMenu menu;
extern GameScene mainscene;
extern GamePhase* game;
extern AudioManager *AM;
extern const struct aiScene* scene;
extern struct aiVector3D scene_min, scene_max, scene_center;
extern float distance;
extern GLuint scene_list;
extern std::map<std::string, GLuint*> textureIdMap;
extern GLuint*	textureIds;
extern int totalnodes;
extern int score;

void get_bounding_box(struct aiVector3D* min, struct aiVector3D* max, aiNode* node);
int loadasset(const char* path);
int InitGL();
int LoadImage(char* path);
void output(int x, int y, char* s);
