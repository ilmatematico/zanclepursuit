#include "GameScene.h"
#include "stdfx.h"
#include "global.h"
#include <iostream>
#include "windows.h"


Bomb::Bomb(double x, double y, double z, GameScene* scene)
{
	bombSpawnedPosition = Triton::Vector3(x, y, z);
	initialize(scene);
};
Bomb::Bomb(Triton::Vector3 spawnPosition, GameScene* scene)
{
	bombSpawnedPosition = spawnPosition;
	initialize(scene);
}
Bomb::~Bomb(){};
void Bomb::initialize(GameScene* scene)
{
	this->gameScene = scene;
	decalHandle = gameScene->ocean->AddDecal(reinterpret_cast<Triton::TextureHandle>(scene->bombDecalTextureID), 5.0f, bombSpawnedPosition);
	impact = new Triton::Impact(gameScene->ocean, 2.0, 1000.0, true,5.0);
}
void Bomb::detonate()
{
	double dt = timeGetTime()-(gameScene->startTritonTime);
	impact->Trigger(bombSpawnedPosition, Triton::Vector3(0, -1, 0), 4000.0, dt * 0.001);
	if (decalHandle != nullptr)
		gameScene->ocean->RemoveDecal(decalHandle);	
}
Triton::Vector3 Bomb::getDistance(Triton::Vector3 from)
{
	return Triton::Vector3(); //FIXME
}
Triton::Vector3 Bomb::getPosition()
{
	return bombSpawnedPosition;
}

std::pair<bool, bool> Bomb::checkCollision(Triton::Vector3 pos)
{
	std::pair<bool, bool> boom(false, false);
	aiVector3D min, max;
	get_bounding_box(&min, &max, scene->mRootNode->mChildren[NODO_GUARDIA_COSTIERA]);
	if ((pos.x+min.z*gameScene->modelScale < bombSpawnedPosition.x) && (pos.x + max.z*gameScene->modelScale > bombSpawnedPosition.x) &&
		(pos.z + min.x*gameScene->modelScale < bombSpawnedPosition.z) && (pos.z + max.x*gameScene->modelScale > bombSpawnedPosition.z)) {
		
			detonate();
			AM->playFxExplosion();
			boom.first = true;
	}
	else if(pos.z - bombSpawnedPosition.z > COLLISION_FAR_THRESHOLD)
	{
		detonate();
		AM->playFxExplosion(true);
		boom.second = true;
	}
	return boom;
}
