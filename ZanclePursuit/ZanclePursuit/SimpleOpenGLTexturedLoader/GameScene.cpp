#include "GameScene.h"
#include "stdfx.h"
#include "global.h"
#include <iostream>

GameScene::GameScene()
{
}
GameScene::~GameScene()
{
}
// ----------------------------------------------------------------------------

unsigned long getMilliseconds()
{
#ifdef _WIN32
	return timeGetTime();
#else
	struct timeb tp;
	ftime(&tp);
	static time_t startTime = 0;
	if (startTime == 0) startTime = tp.time;
	return (((tp.time - startTime) * 1000) + tp.millitm);
#endif
}
void CalcTritonMatrices(GLfloat *modelView, GLfloat *projView, Triton::Environment *env)
{
	double mv[16], proj[16];
	for(int i=0, z=0; i<4; i++)
		for(int j = 0; j<4;j++,z++)
		{
			mv[z] = modelView[z];
			proj[z] = projView[z]; //i + j * 4
		}
	// Pass the final view and projection matrices into Triton.
	if (env) {
		env->SetCameraMatrix(mv);
		env->SetProjectionMatrix(proj);
	}
}
GLfloat* getSkyboxMatrix()
{
	// Compute the modelview and perspective matrices, and a pass them into Triton.
	GLfloat  projView[16];// skybox[16], modelView[16] ;
	//glGetFloatv(GL_MODELVIEW_MATRIX, modelView);
	glGetFloatv(GL_PROJECTION_MATRIX, projView); 

	return projView;
}
double GameScene::DrawTritonFrame()
{
	// Compute the modelview and perspective matrices, and a pass them into Triton.
	GLfloat modelView[16], projView[16];
	glGetFloatv(GL_MODELVIEW_MATRIX, modelView);
	glGetFloatv(GL_PROJECTION_MATRIX, projView);
	CalcTritonMatrices(modelView, projView, environment);

	// Clear the depth buffer
	if (ocean->IsCameraAboveWater()) {
		glClearDepth(1.0);
		glClear(GL_DEPTH_BUFFER_BIT);
		glClearColor(0.0, 0.0, 0.0, 1.0);
	}
	else {
		glClearColor(0.0, 0.1, 0.3, 1.0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}
	// Now light and draw the ocean:
	if (environment) {
		// The sun position is roughly where it is in our skybox texture:
		Triton::Vector3 lightDirection(0, 5, -10);
		environment->SetDirectionalLight(lightDirection, Triton::Vector3(1.0, 1.0, 1.0));

		// Ambient color based on the zenith color in the cube map
		Triton::Vector3 ambientColor(80.0 / 255.0, 160.0 / 255.0, 190.0 / 255.0);
		ambientColor.Normalize();
		environment->SetAmbientLight(ambientColor);

		environment->SetBelowWaterVisibility(100.0, Triton::Vector3(0.0, 0.1, 0.3));

		// Grab the cube map from our sky box and give it to Triton to use as an environment map
		environment->SetEnvironmentMap((Triton::TextureHandle)(skybox->GetCubemap()));

		// Draw the ocean for the current time sample
		if (ocean) {
			static double startMillis = 0;
			double millis = timeGetTime();
			if (startMillis == 0) {
				startMillis = millis;
			}
			millis = millis - startMillis;
			glPushMatrix();
				ocean->Draw(millis * 0.001);
			glPopMatrix();
			return startMillis;
		}	
	}
}
// ----------------------------------------------------------------------------

void color4_to_float4(const struct aiColor4D *c, float f[4])
{
	f[0] = c->r;
	f[1] = c->g;
	f[2] = c->b;
	f[3] = c->a;
}

void set_float4(float f[4], float a, float b, float c, float d)
{
	f[0] = a;
	f[1] = b;
	f[2] = c;
	f[3] = d;
}

// Can't send color down as a pointer to aiColor4D because AI colors are ABGR.
void Color4f(const struct aiColor4D *color)
{
	glColor4f(color->r, color->g, color->b, color->a);
}

void apply_material(const struct aiMaterial *mtl)
{
	float c[4];

	GLenum fill_mode;
	int ret1, ret2;
	struct aiColor4D diffuse;
	struct aiColor4D specular;
	struct aiColor4D ambient;
	struct aiColor4D emission;
	float shininess, strength;
	int two_sided;
	int wireframe;
	int max;

	int texIndex = 0;
	aiString texPath;	//contains filename of texture
	if (AI_SUCCESS == mtl->GetTexture(aiTextureType_DIFFUSE, texIndex, &texPath))
	{
		//bind texture
		unsigned int texId = *textureIdMap[texPath.data];
		glBindTexture(GL_TEXTURE_2D, texId);
	}

	set_float4(c, 0.8f, 0.8f, 0.8f, 1.0f);
	if (AI_SUCCESS == aiGetMaterialColor(mtl, AI_MATKEY_COLOR_DIFFUSE, &diffuse))
		color4_to_float4(&diffuse, c);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c);

	set_float4(c, 0.0f, 0.0f, 0.0f, 1.0f);
	if (AI_SUCCESS == aiGetMaterialColor(mtl, AI_MATKEY_COLOR_SPECULAR, &specular))
		color4_to_float4(&specular, c);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, c);

	set_float4(c, 0.2f, 0.2f, 0.2f, 1.0f);
	if (AI_SUCCESS == aiGetMaterialColor(mtl, AI_MATKEY_COLOR_AMBIENT, &ambient))
		color4_to_float4(&ambient, c);
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, c);

	set_float4(c, 0.0f, 0.0f, 0.0f, 1.0f);
	if (AI_SUCCESS == aiGetMaterialColor(mtl, AI_MATKEY_COLOR_EMISSIVE, &emission))
		color4_to_float4(&emission, c);
	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, c);

	max = 1;
	ret1 = aiGetMaterialFloatArray(mtl, AI_MATKEY_SHININESS, &shininess, (unsigned int *)&max);
	max = 1;
	ret2 = aiGetMaterialFloatArray(mtl, AI_MATKEY_SHININESS_STRENGTH, &strength, (unsigned int *)&max);
	if ((ret1 == AI_SUCCESS) && (ret2 == AI_SUCCESS))
		glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shininess * strength);
	else {
		glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 0.0f);
		set_float4(c, 0.0f, 0.0f, 0.0f, 0.0f);
		glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, c);
	}

	max = 1;
	if (AI_SUCCESS == aiGetMaterialIntegerArray(mtl, AI_MATKEY_ENABLE_WIREFRAME, &wireframe, (unsigned int *)&max))
		fill_mode = wireframe ? GL_LINE : GL_FILL;
	else
		fill_mode = GL_FILL;
	glPolygonMode(GL_FRONT_AND_BACK, fill_mode);

	max = 1;
	if ((AI_SUCCESS == aiGetMaterialIntegerArray(mtl, AI_MATKEY_TWOSIDED, &two_sided, (unsigned int *)&max)) && two_sided)
		glEnable(GL_CULL_FACE);
	else
		glDisable(GL_CULL_FACE);
}

void recursive_render(const struct aiScene *sc, const struct aiNode* nd, float scale)
{
	unsigned int i;
	unsigned int n = 0, t;
	struct aiMatrix4x4 m = nd->mTransformation;

	// update transform
	m.Transpose();
	glPushMatrix();
	glMultMatrixf((float*)&m);

	// draw all meshes assigned to this node
	for (; n < nd->mNumMeshes; ++n)
	{
		const struct aiMesh* mesh = scene->mMeshes[nd->mMeshes[n]];

		apply_material(sc->mMaterials[mesh->mMaterialIndex]);


		if (mesh->HasTextureCoords(0))
			glEnable(GL_TEXTURE_2D);
		else
			glDisable(GL_TEXTURE_2D);
		if (mesh->mNormals == NULL)
		{
			glDisable(GL_LIGHTING);
		}
		else
		{
			glEnable(GL_LIGHTING);
		}

		if (mesh->mColors[0] != NULL)
		{
			glEnable(GL_COLOR_MATERIAL);
		}
		else
		{
			glDisable(GL_COLOR_MATERIAL);
		}

		for (t = 0; t < mesh->mNumFaces; ++t) {
			const struct aiFace* face = &mesh->mFaces[t];
			GLenum face_mode;

			switch (face->mNumIndices)
			{
			case 1: face_mode = GL_POINTS; break;
			case 2: face_mode = GL_LINES; break;
			case 3: face_mode = GL_TRIANGLES; break;
			default: face_mode = GL_POLYGON; break;
			}

			glBegin(face_mode);
			for (i = 0; i < face->mNumIndices; i++)		// go through all vertices in face
			{
				int vertexIndex = face->mIndices[i];	// get group index for current index
				if (mesh->mColors[0] != NULL)
					Color4f(&mesh->mColors[0][vertexIndex]);
				if (mesh->mNormals != NULL)

				if (mesh->HasTextureCoords(0))		//HasTextureCoords(texture_coordinates_set)
				{
					glTexCoord2f(mesh->mTextureCoords[0][vertexIndex].x, 1 - mesh->mTextureCoords[0][vertexIndex].y); //mTextureCoords[channel][vertex]
				}

				glNormal3fv(&mesh->mNormals[vertexIndex].x);
				glVertex3fv(&mesh->mVertices[vertexIndex].x);
			}
			glEnd();

		}
	}
	// draw all children
	for (n = 0; n < nd->mNumChildren; ++n)
	{
		recursive_render(sc, nd->mChildren[n], scale);
	}
	glPopMatrix();
}

void GameScene::idle()
{

	static GLint prev_time = glutGet(GLUT_ELAPSED_TIME);

	double time = glutGet(GLUT_ELAPSED_TIME);

	frametime = time - prev_time;
	timer += frametime;
	//virataMultiplyer = ik;
	
	if (timer >= 1000 && !finished) {//conta i secondi
		if(!gcIntro)second++;
		if (second < 0) {
			maxspeed = 0;
			speed = 0;
			right = false;
			left = false;
			up = false;
			down = false;
			space = false;
			finished = true;
			endtype = 0;
		}
		timer = 0;
	}
	
	if (speed < maxspeed)//accelerazione
		speed += 0.001f;
	else if (speed > maxspeed)
	{
		if (maxspeed == 0)
			speed -= 0.0025f; //decelerazione graduale
		else
			speed -= 0.001f;
	}
	if (gcIntro) {
		if (skipIntro)
			x_coord_gc = 0.0;
		if(x_coord_gc < 0.0)
			right = true;
		else
		{
			gcIntro = right = skipIntro = false;
			gcGo = true;
			AM->playFxSiren();
		}
		left = leftArrow = rightArrow = up = down = spawnBomb = false;
	}
	
	if (finished) {
		maxspeed = 0;
		right = left = up = down = leftArrow = rightArrow = false;
		if(playerBoat) //Affonda Gc
		{
			AM->stopFxSiren();
			gc.y -= frametime * 0.005f;
			if (xangle < 60) {
				if (xangle < 0)
					xangle += frametime*0.02f;
				else
					xangle += frametime*0.03f;
			}
			if (gc.y < -25) playerBoat = false;

		}
		else if(playerGc){
			AM->playFxMegaphone();
		}
	}
	else {
		maxspeed = 0.03;//0.018;
		distance += (frametime)*speed;
	}
	if(gcGo)
		z_coord_gc += frametime*speed*gcSpeedMultyplier; //GC GO!

	if (right) {
		if (z_angle_gc < 25) {
			if (z_angle_gc < 0)
				z_angle_gc += frametime*0.2f;
			else
				z_angle_gc += frametime*0.05f;
		}
		x_coord_gc += frametime*speed*virataMultyplier;
	}
	else if (left) {
		if (z_angle_gc > -25) {
			if (z_angle_gc > 0)
				z_angle_gc -= frametime*0.2f;
			else
				z_angle_gc -= frametime*0.05f;
		}
		x_coord_gc -= frametime*speed*virataMultyplier;
	}
	else {//rientro in assetto rispetto asse z
		if (z_angle_gc > 0)
			z_angle_gc -= frametime*0.06f;
		if (z_angle_gc < 0)
			z_angle_gc += frametime*0.06f;
	}
	//up-down indipendenti da left-right
	if (down) {
		if (xangle < 5) {
			if (xangle < 0)
				xangle += frametime*0.02f;
			else
				xangle += frametime*0.03f;
		}
		z_coord_gc += frametime*0.0015f;
	}
	else if (up) {
		if (xangle > -5) {
			if (xangle > 0)
				xangle -= frametime*0.02f;
			else
				xangle -= frametime*0.03f;
		}
		z_coord_gc -= frametime*0.001f;
	}
	else if (!playerBoat) {//rientro in assetto rispetto asse x	
		if (xangle > 0)
			xangle -= frametime*0.06f;
		if (xangle < 0)
			xangle += frametime*0.06f;
	}
	//BOAT MOVE
	if(leftArrow)
	{
		if (z_angle_boat > -15) {
			if (z_angle_boat > 0)
				z_angle_boat -= frametime*0.2f;
			else
				z_angle_boat -= frametime*0.05f;
		}
		x_coord_boat -= frametime*speed*virataMultyplier;
	}
	else if(rightArrow)
	{
		if (z_angle_boat < 15) {
			if (z_angle_boat < 0)
				z_angle_boat += frametime*0.2f;
			else
				z_angle_boat += frametime*0.05f;
		}
		x_coord_boat += frametime*speed*virataMultyplier;
	}
	else{
		if (z_angle_boat > 0)
			z_angle_boat -= frametime*0.06f;
		if (z_angle_boat < 0)
			z_angle_boat += frametime*0.06f;
	}
	prev_time = time;
	
	//SEA LEVEL
	if (y_up) {
		seaLevel += 0.2;
		environment->SetSeaLevel(seaLevel);
	}
	else if (y_down) {
		seaLevel -= 0.2;
		environment->SetSeaLevel(seaLevel);
	}
	//CONTROLLO ALTEZZA DI VOLO
	if (z_coord_gc >= 500) {
		maxspeed = 0;
		speed = 0;
		right = false;
		left = false;
		up = false;
		down = false;
		space = false;
		z_coord_gc = 499;
		second = 0;
		finished = true;
		endtype = 1;
	}

	//DISEGNA PUNTI ACQUISITI
	if (disegnaPunti) {
		posizionePunti += 5;
		if (posizionePunti >= 120) {
			posizionePunti = 0;
			disegnaPunti = false;
		}
	}
	//BOMB DECAL
	if (spawnBomb) {
		spawnBomb = false;
		if (bombarolo.spawn()) {
			Triton::Vector3 bomb(boat); bomb.z += distance;
			bombList.push_back(Bomb(bomb, this));
		}
	}
	if (bombList.size() != 0)
		bombList.remove_if([&](Bomb b)->bool {
		auto r = b.checkCollision(Triton::Vector3(gc.x, gc.y, gc.z + distance));
		if (r.first) {
			finished = true;
			playerBoat = true;
		}
		return r.first || r.second;
	});
	checkCatch();
	glutPostRedisplay();
}

void GameScene::render() {

	float tmp;
	int w = glutGet(GLUT_WINDOW_WIDTH);
	int h = glutGet(GLUT_WINDOW_HEIGHT);
	cameraPosition = Triton::Vector3(boat.x, zx + 10, cv + 111);
	//SCENA 3D
	glEnable(GL_COLOR_MATERIAL);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(cameraPosition.x, cameraPosition.y, cameraPosition.z + distance,
		gc.x, 0.5, gc.z + distance,
		0.f, 1.f, 0.f);

	startTritonTime = DrawTritonFrame();
	glLoadIdentity();
	gluLookAt(cameraPosition.x, cameraPosition.y, cameraPosition.z,
		gc.x, 0.5, gc.z,
		0.f, 1.f, 0.f);
	
	GLfloat* sbm = getSkyboxMatrix();
	GLfloat m[16];
	for (int i = 0; i < 16; i++) m[i] = sbm[i];

	boat.x = x_coord_boat;
	bombarolo.setX(x_coord_boat);

	gc.z = cameraTarget.z + z_coord_gc;
	gc.x = cameraTarget.x + x_coord_gc;
	//GC
	glPushMatrix();
		glTranslatef(gc.x, gc.y, gc.z);
		glRotatef(-90, 0.f, 1.f, 0.f);
		glRotatef(z_angle_gc, 0, 1, 0);
		glRotatef(z_angle_gc / 3, 1, 0, 0);
		glRotatef(xangle, 0, 0, 1);
		glScalef(modelScale, modelScale, modelScale);
		recursive_render(scene, scene->mRootNode->mChildren[NODO_GUARDIA_COSTIERA], 1.0);
	glPopMatrix();
	
	//SCIA GC
	Triton::Vector3 actualGcWake(gc); actualGcWake.z += distance-16;
	if (finished)
	{
		gcWake->ClearWakes();
		if(playerBoat) washSpeed = 10.0;
		if (!playerBoat && !playerGc && washSpeed>0.0)
			washSpeed -= frametime*0.002;
		actualGcWake = gc;
		actualGcWake.y = 1; actualGcWake.z += distance;
		affondaWash->Update(actualGcWake, Triton::Vector3(0, -1, 0), 
			washSpeed, (timeGetTime() - startTritonTime)*0.001);
		
	}
	gcWake->Update(actualGcWake, Triton::Vector3(0, 0, 1), finished?0:14, (timeGetTime()-startTritonTime)*0.001);

	//BOAT
	glPushMatrix();
		glTranslatef(boat.x, boat.y, boat.z);
		glRotatef(z_angle_boat, 0, 1, 0);
		//glRotatef(z_angle_boat / 3, 1, 0, 0);
		glScalef(modelScale, modelScale, modelScale);
		recursive_render(scene, scene->mRootNode->mChildren[NODO_BOAT], 1.0);
	glPopMatrix();

	//SCIA BOAT
	Triton::Vector3 actualBoatWake(boat); actualBoatWake.z += distance + 8;
	if(finished) boatWake->ClearWakes();
		boatWake->Update(actualBoatWake, Triton::Vector3(0, 0, 1), finished?1:12, (timeGetTime() - startTritonTime)*0.001);
	bombarolo.render();
	
	// Draw our skybox last to take advantage of early depth culling
	if (skybox && ocean->IsCameraAboveWater()) {
		skybox->Draw(m);
	}
//___________________________________________________________________________________________________
	if (scene_list == 0) {
		scene_list = glGenLists(1);
		glNewList(scene_list, GL_COMPILE);
		glEndList();
	}
	glCallList(scene_list);
	glActiveTexture(GL_TEXTURE2);
	/////HUD 2D
	glPushMatrix();
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glClear(GL_DEPTH_BUFFER_BIT);

	glOrtho(0.0, w, h, 0.0, -1.0, 10.0);
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
	glLoadIdentity();
	glDisable(GL_CULL_FACE);
	glDisable(GL_LIGHTING);

	if (!finished)
	{//disegna tempo, punteggio e turbo durante il gioco
		char buffer[10];

		glColor3ub(66, 66, 66);
		glBegin(GL_QUADS); //Bombe rimaste
		glVertex2f(0, 10);
		glVertex2f(0, 60);
		glVertex2f(120, 60);
		glVertex2f(120, 10);
		glEnd();

		glBegin(GL_QUADS); //SFONDOSCORE
		glVertex2f(w, 10);
		glVertex2f(w, 60);
		glVertex2f(w - 120, 60);
		glVertex2f(w - 120, 10);
		glEnd();

		if (gcIntro) {
			static double offset = 60;
			glBegin(GL_QUADS); //sfondo SKIP INTRO
			glVertex2f(0, h	- offset);
			glVertex2f(0, h-40-offset);
			glVertex2f(190, h-40 - offset);
			glVertex2f(190, h-offset);
			glEnd();
			glColor3ub(51, 153, 255);
			output(10, h-15-offset, "SPACE to skip intro");
		}

		glColor3ub(51, 153, 255);
		output(10, 30, "Bombs left");
		output(20, 55, itoa(bombarolo.getBombsLeft(), buffer, 10));
		output(w - 90, 30, "Time");
		//PER CENTRARE LA SCRITTA
		if (score < 1000 && score > 0)
			output(w - 74, 55, itoa(second, buffer, 10));
		else if (score >= 1000)
			output(w - 80, 55, itoa(second, buffer, 10));
		else
			output(w - 66, 55, itoa(second, buffer, 10));
	
	}
	else {	//gioco finito
		char buffer[10];
		glColor4ub(66, 66, 66,50);
		glBegin(GL_QUADS); //SFONDFINISH
		glVertex2f(w / 6, 40);
		glVertex2f(w / 6, 230);
		glVertex2f(w - w / 6, 230);
		glVertex2f(w - w / 6, 40);
		glEnd();
		glColor3ub(255, 255, 255);
		output(w / 2 - 180, 200, "PRESS ENTER TO TRY AGAIN, ESC FOR QUIT");//scritto in ogni caso
		glColor3ub(0, 153, 0);														   //DisegnaPunti acquisiti con movimento
		glColor3f(0, 153, 0);
		output(w / 2 - 40, 170, itoa(second, buffer, 10));
		output(w / 2 -5, 170, "seconds");
		if (!playerGc)	{
			output(w / 2 - 40, 100, "Player Boat WIN!");
		}
		else {
			output(w / 2 - 100, 100, "Player Coast Guard WIN!");
			output(w / 2 - 45, 140, "Catched");
		}
	}

	//TORNO 3D
	glMatrixMode(GL_PROJECTION);
	glEnable(GL_CULL_FACE);
	glEnable(GL_LIGHTING);
	glDisable(GL_COLOR_MATERIAL);
	glPopMatrix();
	glActiveTexture(GL_TEXTURE0);

	//FINE
	glutSwapBuffers();
}

void GameScene::reshape(int w, int h){
	const double aspectRatio = (float)w / h, fieldOfView = 45.0;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(fieldOfView, aspectRatio, 0.3, 6000.0);  // Znear and Zfar 
	glViewport(0, 0, w, h);
}
void GameScene::arrowKeys(int key, int x, int y){
	switch (key) {
	
	case GLUT_KEY_LEFT:
		leftArrow = true;
		break;
	case GLUT_KEY_RIGHT:
		rightArrow = true;
		break;
	case GLUT_KEY_UP:
		//do something here
		break;
	case GLUT_KEY_DOWN:
		//do something here
		break;
	}
}
void GameScene::arrowKeysUp(int key, int x, int y)
{
	switch (key) {

	case GLUT_KEY_LEFT:
		leftArrow = false;
		break;
	case GLUT_KEY_RIGHT:
		rightArrow = false;
		break;
	case GLUT_KEY_UP:
		//do something here
		break;
	case GLUT_KEY_DOWN:
		//do something here
		break;
	}
}
void GameScene::keyboard(unsigned char key, int x, int y){
	if (speed > 0){
		switch (key) {
			std::cout << "Key Pressed" << key << "("<< timer <<">"<< std::endl;
		case 'z':
			zx += 1.0;
			break;
		case 'x':
			zx -= 1.0;
			break;
		case 'o':
			ol += 1.0;
			break;
		case 'l':
			ol -= 1.0;
			break;
		case 'i':
			ik += .1;
			break;
		case 'k':
			ik -= .1;
			break;
		case 'c':
			cv += 1.0;
			break;
		case 'v':
			cv -= 1.0;
			break;
		case 'u':
			y_up = true;
			break;
		case 'j':
			y_down = true;
			break;
		case 'd':
			right = true;
			break;
		case 'a':
			left = true;
			break;
		case 'w':
			down = true;
			break;
		case 's':
			up = true;
			break;
		case 32: //spazio
			//space = true;
			break;
		case 27: //esc
			exit(1);
			break;
		default:
			break;
		}
	}
	else if (finished){//se il gioco � terminato impedisce i movimenti
		switch (key){
		case 27://esc
			exit(1);
			break;
		case 13://invio
			//RINIZIA
			startgame();
		}
	}
}
void GameScene::keyboardup(unsigned char key, int x, int y){//si attiva sul rilascio del tasto
	if (speed > 0){
		switch (key) {
		case 'u':
			y_up = false;
			break;
		case 'j':
			y_down = false;
			break;
		case 'd':
			right = false;
			break;
		case 'a':
			left = false;
			break;
		case 'w':
			down = false;
			break;
		case 's':
			up = false;
			break;
		case '-': //spazio 32
			spawnBomb = true;
			break;
		case 32:
			skipIntro = true;
			break;
		default:
			break;
		}
	}
}

void GameScene::startgame(){//inizializza
	AM->playSoundtrackMain();
	AM->stopFxSiren();
	minfuel = 22;
	maxfuel = 199;
	fuel = maxfuel-1;
	finished = skipIntro = playerGc =  playerBoat = false;
	win = false;
	gcIntro = true; gcGo = false,
	endtype = -1;
	distance = 0.f;
	x_coord_gc = GC_INTRO_X;
	z_coord_gc = 0;
	x_coord_boat = 0.0;
	z_angle_gc = 0.0;
	z_angle_boat = 0.0;
	xangle = 4.8;
	yrot = 0.0;
	xrot = 0.0;
	speed = 0.002;
	ncollision = 0;
	timer = 0;
	score = 0;
	second = 0;
	maxspeed = 0.017;
	seaLevel = 0.0;
	zx = cv = ik = 0.0;
	cameraPosition = Triton::Vector3(0.f, 10.f, 111.f);
	cameraTarget = Triton::Vector3(0, 0, START_GC_DISTANCE);
	modelScale = 5.f;
	gc = cameraTarget; gc.y = -0.5;
	boat = cameraTarget; boat.z = cameraPosition.z-4;
	bombarolo = Spawner(boat.x, 5, boat.z-17,nullptr);
	environment->SetSeaLevel(seaLevel);
	startTritonTime = 0;
	//environment->SetCurrent(Triton::Vector3(0,0,1), 20.0);
	Triton::WakeGeneratorParameters parametersGc, parametersBoat;
		parametersGc.sprayEffects = true;
		parametersGc.beamWidth = 7.0;
		parametersGc.length = 6.0;
		parametersGc.bowWave = true;
		parametersGc.sternWaves = true;
		parametersGc.sternWaveOffset = +20;
		parametersGc.sternWakeMultiplier = 5.0;
		parametersGc.propWash = true;
	gcWake = new Triton::WakeGenerator(ocean, parametersGc);
		parametersBoat.sprayEffects = false;
		parametersBoat.sternWaves = true;
		parametersBoat.sternWaveOffset = -16;
		parametersBoat.beamWidth = 4.0;
		parametersBoat.propWash = true;
		parametersBoat.bowWave = false;
		parametersBoat.length = 4.0;
	boatWake = new Triton::WakeGenerator(ocean, parametersBoat);
	affondaWash = new Triton::RotorWash(ocean, 25, false, true);
	washSpeed = 0.0;
	bombarolo.reset();
	bombList.clear();
}

bool GameScene::checkCatch()
{
	aiVector3D minG, minB, maxG, maxB;
	get_bounding_box(&minG, &maxG, scene->mRootNode->mChildren[NODO_GUARDIA_COSTIERA]);
	get_bounding_box(&minB, &maxB, scene->mRootNode->mChildren[NODO_GUARDIA_COSTIERA]);
	/*
	 double a, b, c,d,e,f,g;
	a = (gc.x + modelScale*maxG.z);
	b = (boat.x + modelScale*minB.z);
	d = (gc.x + modelScale*minG.z);
	e = (boat.x + modelScale*maxB.z);
	c = std::abs(a - e);
	f = std::abs(b-d);
	g = std::abs(gc.x - boat.x);
	std::cout << a << " / " << b << " / "<< e << " / "
		<< d << " // " << c << " / " << f  <<" / "<< g << std::endl;
	*/
	if (std::abs((gc.z + modelScale*maxG.x) - (boat.z + modelScale*minB.x)) < CATCH_THRESHOLD) {
		gcGo = false;
		if (std::abs(gc.x - boat.x) < 1.6*modelScale*CATCH_THRESHOLD) {
			finished = true;
			playerGc = true;
			std::cout << "GAME OVER: CATCHED" << std::endl;
			return true;
		}
	}
	return false;
}
