// assimp include files. These three are usually needed.
#include <GL/glew.h>
#include "stdfx.h"
#include "global.h"
#include <windows.h>
//to map image filenames to textureIds
#include <string.h>
#include <map>


GamePhase* game;
GameMenu menu;
GameScene mainscene;

AudioManager *AM;

int totalnodes;

// currently this is hardcoded
//static const std::string basepath = "./models/textures/"; //obj..
static const std::string basepath = "./models/"; //per i file blend

// the global Assimp scene object
const struct aiScene* scene = NULL;
GLuint scene_list = 0;
struct aiVector3D scene_min, scene_max, scene_center;

// current rotation angle
float distance = -200.f;

// images / texture
std::map<std::string, GLuint*> textureIdMap;	// map image filenames to textureIds
GLuint*		textureIds;							// pointer to texture Array

GLfloat LightAmbient[]= { 100.f, 100.f,100.f, 1.0f };
GLfloat LightDiffuse[]= { 2.f, 2.0f, 2.0f, 1.0f };
GLfloat LightSpecular[] = { 1.f, 1.f, 1.f, 1.f };
GLfloat LightPosition[]= { 2.0f, 3.0f, 1.0f, 1.0f };

int score = 0;

// The three main Triton objects you need:
static Triton::ResourceLoader *resourceLoader;
static Triton::Environment *environment;
static Triton::Ocean *ocean;
static SkyBox *skyBox;

std::string current_working_directory()
{
	char working_directory[MAX_PATH + 1];
	GetCurrentDirectoryA(sizeof(working_directory), working_directory); // **** win32 specific ****
	return working_directory;
}
//_______________________________________________________________________________________
bool InitTriton()
{
	std::string path = current_working_directory() + std::string("\\Resources\\");
	resourceLoader = new Triton::ResourceLoader(path.c_str());

	// Create an environment for the water, with a flat-Earth coordinate system with Y
	// pointing up and using an OpenGL 2.0 capable context.
	environment = new Triton::Environment();
	Triton::EnvironmentError err = environment->Initialize(Triton::FLAT_YUP,
		Triton::OPENGL_2_0, resourceLoader);

	if (err != Triton::SUCCEEDED) {
#ifdef _WIN32
		::MessageBoxA(NULL, "Failed to initialize Triton - is the resource path passed in to "
			"the ResouceLoader constructor valid?", "Triton error", MB_OK | MB_ICONEXCLAMATION);
#else
		printf("Failed to initialize Triton.\n");
#endif
		return false;
	}
	
	environment->SetLicenseCode("Your license name", "Your license code");
	// Set up wind of 15 m/s blowing roughly from the North
	environment->SimulateSeaState(4, 0.0);
	environment->SetDouglasSeaScale(3,0.3,1,0.4);
	
	ocean = Triton::Ocean::Create(environment, Triton::JONSWAP,true,AI_TRUE);
	if (ocean) {
		printf("Using FFT Technique %s\n", ocean->GetFFTName());
		// If you have GPU acceleration you can probably get away with higher quality...
		if (strstr(ocean->GetFFTName(), "CUDA") != 0) {
			ocean->SetQuality(Triton::GOOD);
		}
		//Disabling spray will improve performance, if you don't need it.
		ocean->EnableSpray(false);
	}
	skyBox = new SkyBox();
	return (ocean != NULL);
}
//_______________________________________________________________________________________
void idle (void)
{
	if (game != nullptr)
		game->idle();
	
}
void display(void)
{
	if(game!=nullptr)
		game->render();
}
void reshape(int width, int height)
{
	if (game != nullptr)
		game->reshape(width, height);

}
void keyboard(unsigned char key, int x, int y){
	if (game != nullptr)
		game->keyboard(key,x,y);

}
void keyboardup(unsigned char key, int x, int y){
	if (game != nullptr)
		game->keyboardup(key, x, y);
}
void arrowKeys(int key, int x, int y){
	if (game != nullptr)
		game->arrowKeys(key, x, y);
}
void arrowKeysUp(int key, int x, int y) {
	if (game != nullptr)
		game->arrowKeysUp(key, x, y);
}

//_______________________________________________________________________________________

void get_bounding_box_for_node(const struct aiNode* nd, struct aiVector3D* min, struct aiVector3D* max, struct aiMatrix4x4* trafo) {
	struct aiMatrix4x4 prev;
	unsigned int n = 0, t;

	prev = *trafo;
	aiMultiplyMatrix4(trafo, &nd->mTransformation);

	for (; n < nd->mNumMeshes; ++n) {
		const struct aiMesh* mesh = scene->mMeshes[nd->mMeshes[n]];
		for (t = 0; t < mesh->mNumVertices; ++t) {

			struct aiVector3D tmp = mesh->mVertices[t];
			aiTransformVecByMatrix4(&tmp, trafo);

			min->x = aisgl_min(min->x, tmp.x);
			min->y = aisgl_min(min->y, tmp.y);
			min->z = aisgl_min(min->z, tmp.z);

			max->x = aisgl_max(max->x, tmp.x);
			max->y = aisgl_max(max->y, tmp.y);
			max->z = aisgl_max(max->z, tmp.z);
		}
	}

	for (n = 0; n < nd->mNumChildren; ++n) {
		get_bounding_box_for_node(nd->mChildren[n], min, max, trafo);
	}
	*trafo = prev;
}
void get_bounding_box(struct aiVector3D* min, struct aiVector3D* max, aiNode* node)
{
	struct aiMatrix4x4 trafo;
	aiIdentityMatrix4(&trafo);

	min->x = min->y = min->z = 1e10f;
	max->x = max->y = max->z = -1e10f;
	get_bounding_box_for_node(node, min, max, &trafo);
}
int loadasset (const char* path)
{
	// we are taking one of the postprocessing presets to avoid
	// writing 20 single postprocessing flags here.
	scene = aiImportFile(path,aiProcessPreset_TargetRealtime_MaxQuality);
	
	if (scene) {
		totalnodes = scene->mRootNode->mNumChildren;
		//CENTRA SULL'AEREO
		get_bounding_box(&scene_min,&scene_max, scene->mRootNode->mChildren[totalnodes-1]);
		scene_center.x = (scene_min.x + scene_max.x) / 2.0f;
		scene_center.y = (scene_min.y + scene_max.y) / 2.0f;
		scene_center.z = (scene_min.z + scene_max.z) / 2.0f;
		return 0;
	}
	return 1;
}

int LoadGLTextures(const aiScene* scene)
{
	ILboolean success;
	/* Before calling ilInit() version should be checked. */
	if (ilGetInteger(IL_VERSION_NUM) < IL_VERSION)
	{
		ILint test = ilGetInteger(IL_VERSION_NUM);
		/// wrong DevIL version ///
		std::string err_msg = "Wrong DevIL version. Old devil.dll in system32/SysWow64?";
		char* cErr_msg = (char *) err_msg.c_str();
		
		return -1;
	}
	ilInit(); /* Initialization of DevIL */

	/* getTexture Filenames and Numb of Textures */
	for (unsigned int m=0; m<scene->mNumMaterials; m++)
	{
		int texIndex = 0;
		aiReturn texFound = AI_SUCCESS;

		aiString path;	// filename

		while (texFound == AI_SUCCESS)
		{
			texFound = scene->mMaterials[m]->GetTexture(aiTextureType_DIFFUSE, texIndex, &path);
			textureIdMap[path.data] = NULL; //fill map with textures, pointers still NULL yet
			texIndex++;
		}
	}

	int numTextures = textureIdMap.size();

	/* array with DevIL image IDs */
	ILuint* imageIds = NULL;
	imageIds = new ILuint[numTextures];

	/* generate DevIL Image IDs */
	ilGenImages(numTextures, imageIds); /* Generation of numTextures image names */

	/* create and fill array with GL texture ids */
	textureIds = new GLuint[numTextures];
	glGenTextures(numTextures, textureIds); /* Texture name generation */

	/* get iterator */
	std::map<std::string, GLuint*>::iterator itr = textureIdMap.begin();

	for (int i=0; i<numTextures; i++)
	{

		//save IL image ID
		std::string filename = (*itr).first;  // get filename
		(*itr).second =  &textureIds[i];	  // save texture id for filename in map
		itr++;								  // next texture


		ilBindImage(imageIds[i]); /* Binding of DevIL image name */
		std::string fileloc = basepath + filename;	/* Loading of image */
		success = ilLoadImage((const wchar_t *)fileloc.c_str());

		fprintf(stdout,"Loading Image: %s\n", fileloc.data());	

		if (success) /* If no error occured: */
		{
			success = ilConvertImage(IL_RGB, IL_UNSIGNED_BYTE); /* Convert every colour component into
			unsigned byte. If your image contains alpha channel you can replace IL_RGB with IL_RGBA */
			if (!success)
			{
				/* Error occured */
				fprintf(stderr,"Couldn't convert image");
				return -1;
			}
			//glGenTextures(numTextures, &textureIds[i]); /* Texture name generation */
			glBindTexture(GL_TEXTURE_2D, textureIds[i]); /* Binding of texture name */
			//redefine standard texture values
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); /* We will use linear
			interpolation for magnification filter */
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); /* We will use linear
			interpolation for minifying filter */
			glTexImage2D(GL_TEXTURE_2D, 0, ilGetInteger(IL_IMAGE_BPP), ilGetInteger(IL_IMAGE_WIDTH),
				ilGetInteger(IL_IMAGE_HEIGHT), 0, ilGetInteger(IL_IMAGE_FORMAT), GL_UNSIGNED_BYTE,
				ilGetData()); /* Texture specification */
		}
		else
		{
			/* Error occured */			
			fprintf(stderr,"Couldn't load Image: %s\n", fileloc.data());
		}
	}
	ilDeleteImages(numTextures, imageIds); /* Because we have already copied image data into texture data
	we can release memory used by image. */

	//Cleanup
	delete [] imageIds;
	imageIds = NULL;

	//return success;
	return TRUE;
}

int InitGL()					 // All Setup For OpenGL goes here
{
	if (!LoadGLTextures(scene))
	{
		return FALSE;
	}

	glEnable(GL_TEXTURE_2D);
	glShadeModel(GL_SMOOTH);		 // Enables Smooth Shading
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0f);				// Depth Buffer Setup
	glEnable(GL_DEPTH_TEST);		// Enables Depth Testing
	glDepthFunc(GL_LEQUAL);			// The Type Of Depth Test To Do
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really Nice Perspective Calculation

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);    // Uses default lighting parameters
	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
	glEnable(GL_NORMALIZE);

	glLightfv(GL_LIGHT1, GL_AMBIENT, LightAmbient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, LightDiffuse);
	glLightfv(GL_LIGHT1, GL_POSITION, LightPosition);
	glLightfv(GL_LIGHT1, GL_SPECULAR, LightSpecular);
	glEnable(GL_LIGHT1);

	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
	glEnable(GL_COLOR_MATERIAL);

	glewInit();

	return TRUE;					// Initialization Went OK
}
void Destroy()
{
	if (ocean) delete ocean;
	if (environment) delete environment;
	if (resourceLoader) delete resourceLoader;
	//if (skyBox) delete skyBox;
}
// ----------------------------------------------------------------------------
int main(int argc, char **argv)
{
	ILuint image;
	GLuint texidsfondo;

	glutInitWindowSize(1920,1080);
	glutInitWindowPosition(0,0);
	glutInitDisplayMode(GLUT_RGBA| GLUT_DOUBLE | GLUT_DEPTH);
	glutInit(&argc, argv);
	glutCreateWindow("Zancle Pursuit");
	glutFullScreen();

	if (!InitTriton()) {
		return 0;
	}
	mainscene.resourceLoader = resourceLoader;
	mainscene.ocean = ocean;
	mainscene.environment = environment;
	mainscene.skybox = skyBox;
	

	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutIdleFunc(idle);
	glutKeyboardFunc(keyboard);
	glutKeyboardUpFunc(keyboardup);
	glutSpecialFunc(arrowKeys);
	glutSpecialUpFunc(arrowKeysUp);

	ilInit();

	glutGet(GLUT_ELAPSED_TIME);
	
	std::string pathImageBombDecal = current_working_directory() + "\\Models\\textures\\bombDecal.png";
	/* load the file picture with DevIL */
	image = LoadImage(const_cast<char*>(pathImageBombDecal.c_str()));
	if (image == -1)
	{
		printf("Can't load picture file %s by DevIL \n", argv[1]);
		return -1;
	}

	/* OpenGL texture binding of the image loaded by DevIL  */
	glGenTextures(1, reinterpret_cast<GLuint*>(&mainscene.bombDecalTextureID)); /* Texture name generation */
	glBindTexture(GL_TEXTURE_2D, mainscene.bombDecalTextureID); /* Binding of texture name */
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); /* We will use linear interpolation for magnification filter */
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); /* We will use linear interpolation for minifying filter */
	glTexImage2D(GL_TEXTURE_2D, 0, ilGetInteger(IL_IMAGE_BPP), ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT),
		0, ilGetInteger(IL_IMAGE_FORMAT), GL_UNSIGNED_BYTE, ilGetData()); /* Texture specification */


	std::string pathImage = current_working_directory() + "\\Models\\stretto.png";
	/* load the file picture with DevIL */
	image = LoadImage(const_cast<char*>(pathImage.c_str()));
	if (image == -1)
	{
		printf("Can't load picture file %s by DevIL \n", argv[1]);
		return -1;
	}

	/* OpenGL texture binding of the image loaded by DevIL  */
	glGenTextures(1, &texidsfondo); /* Texture name generation */
	glBindTexture(GL_TEXTURE_2D, texidsfondo); /* Binding of texture name */
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); /* We will use linear interpolation for magnification filter */
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); /* We will use linear interpolation for minifying filter */
	glTexImage2D(GL_TEXTURE_2D, 0, ilGetInteger(IL_IMAGE_BPP), ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT),
		0, ilGetInteger(IL_IMAGE_FORMAT), GL_UNSIGNED_BYTE, ilGetData()); /* Texture specification */
	
	AM = new AudioManager();
	game = &menu; 
	game->startgame();
	
	glutMainLoop();

	aiReleaseImport(scene);
	Destroy();
	aiDetachAllLogStreams();	
	return 0;
}

int LoadImage(char *filename)
{
	ILboolean success;
	ILuint image;

	ilGenImages(1, &image); /* Generation of one image name */
	ilBindImage(image); /* Binding of image name */
	success = ilLoadImage((const wchar_t *)filename); /* Loading of the image filename by DevIL */

	if (success) /* If no error occured: */
	{
		/* Convert every colour component into unsigned byte. If your image contains alpha channel you can replace IL_RGB with IL_RGBA */
		success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);

		if (!success)
		{
			return -1;
		}
	}
	else
		return -1;

	return image;
}

void output(int x, int y, char *string)
{
	int len, i;
	void *font = GLUT_BITMAP_HELVETICA_18;

	len = (int)strlen(string);
	glRasterPos2f(x, y);
	for (i = 0; i < len; i++) {
		glutBitmapCharacter(font, string[i]);
	
	}

}
