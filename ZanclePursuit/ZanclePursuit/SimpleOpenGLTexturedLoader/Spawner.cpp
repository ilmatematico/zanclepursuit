#include "GameScene.h"
#include "stdfx.h"
#include "global.h"
#include <iostream>
#include "windows.h"

Spawner::Spawner(Triton::Vector3 p, GameScene* scene)
{
	spawnerPosition = p;
}
Spawner::Spawner(const Spawner& src)
{
	this->spawnerPosition = src.spawnerPosition;
}
Spawner& Spawner::operator=(const Spawner& src)
{
	if(this!=&src)
		this->spawnerPosition = src.spawnerPosition;
	return *this;
}
Spawner::Spawner(double x, double y, double z, GameScene* scene)
{
	spawnerPosition = Triton::Vector3(x, y, z);
}
Spawner::~Spawner(){}
Spawner::Spawner() {}

void Spawner::render()
{
	bombFalling();
	if (bombCounter>0 && 
		(isFalling || timer.getElapsedTime().asSeconds() > SPAWN_RATE)) {
		glDisable(GL_LIGHTING);
		//GLfloat red[] = {1.f,0.f,0.f,1.f};
		//glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, red);
		glPushMatrix();
		glTranslatef(spawnerPosition.x, spawnerPosition.y, spawnerPosition.z);
		glColor3f(67.f/255, 56.f/255, 48.f/255);
		glutSolidSphere(0.5, 16, 10);
		glPopMatrix();
		glEnable(GL_LIGHTING);
	}
}

void Spawner::bombFalling()
{
	if (isFalling) {
		spawnerPosition.y = SPAWN_HEIGTH - std::pow(fallT.getElapsedTime().asSeconds(), 2)*5;
	}
	if (spawnerPosition.y <= 0.0) {
		spawnerPosition.y = SPAWN_HEIGTH;
		isFalling = false;
	}
	if (!isReady && timer.getElapsedTime().asSeconds() > SPAWN_RATE) {
		isReady = true;
		AM->playSpawnerReady();
	}
}

bool Spawner::spawn()
{
	if (bombCounter >0 && timer.getElapsedTime().asSeconds() > SPAWN_RATE) {
		timer.restart();
		fallT.restart();
		isFalling = true;
		isReady = false;
		AM->playBombDrop();
		bombCounter--;
		return true;
	}
	return false;
}

void Spawner::setPosition(Triton::Vector3 pos)
{
	spawnerPosition = pos;
}

void Spawner::setX(double x)
{
	spawnerPosition.x = x;
}

Triton::Vector3 Spawner::getPosition()
{
	return spawnerPosition;
}

void Spawner::reset()
{
	bombCounter = BOMBS;
}

int Spawner::getBombsLeft()
{
	return bombCounter;
} 