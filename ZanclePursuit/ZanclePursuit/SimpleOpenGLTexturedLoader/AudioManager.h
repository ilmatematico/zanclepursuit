#pragma once
#include "stdfx.h"
#include <SFML/Audio.hpp>

#define SOUNDTRACK_MAIN_NUMBER 3
class AudioManager
{
public:
	AudioManager();
	~AudioManager();
	void playSoundtrackMain();
	void playSoundtrackMenu();
	void playFxSiren();
	void playFxMegaphone();
	void playFxExplosion(bool away=false);
	void playBombDrop();
	void playSpawnerReady();
	void stopFxSiren();
private:
	std::string basePath;
	const std::string soundtrackMainPath[SOUNDTRACK_MAIN_NUMBER] = { "101.flac","103.flac","103.flac" };
	const std::string soundtrackMenuPath = "menu.flac";
	const std::string fxAlarmPath = "alarm.wav";
	const std::string fxExplosionPath = "explosion.flac";
	const std::string fxExplosionFarPath = "explosion_far.wav";
	const std::string fxMegaphonePath = "megaphone.flac";
	const std::string fxSpanwerReadyPath = "spawner_ready.flac";
	const std::string fxBombDropPath = "bomb_drop.flac";

	float SOUNDTRACK_VOLUME = 40.f;
	sf::Music soundtrack;
	sf::Sound fxSiren, fxBomb, fxMegaphone, 
		fxBombDrop, fxSpawnerReady;
	sf::SoundBuffer fxAlarmBuffer, fxBombBuffer, 
		fxBombFarBuffer, fxMegaphoneBuffer,
			fxSpawnerReadyBuffer, fxBombDropBuffer;
	void stopSoundtrack();

};

