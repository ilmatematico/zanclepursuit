#pragma once
class GamePhase
{
public:
	GamePhase();
	virtual ~GamePhase();

	virtual void render(void) = 0;
	virtual void reshape(int w, int h) = 0;
	virtual void keyboard(unsigned char key, int x, int y) = 0;
	virtual void keyboardup(unsigned char key, int x, int y) = 0;
	virtual void arrowKeys(int key, int x, int y) = 0;
	virtual void arrowKeysUp(int key, int x, int y) = 0;
	virtual void idle() = 0;
	virtual void startgame() = 0;
};

