#include "AudioManager.h"
#include <iostream>

std::string AudioDir()
{
	char working_directory[MAX_PATH + 1];
	GetCurrentDirectoryA(sizeof(working_directory), working_directory); // **** win32 specific ****
	return working_directory + std::string("\\Audio\\");
}

AudioManager::AudioManager()
{
	soundtrack.setVolume(SOUNDTRACK_VOLUME);
	soundtrack.setLoop(true);
	basePath=AudioDir();
	std::cout << "Set Audio path to " << basePath << std::endl;
	fxAlarmBuffer.loadFromFile(basePath + fxAlarmPath);
	fxBombBuffer.loadFromFile(basePath + fxExplosionPath);
	fxBombFarBuffer.loadFromFile(basePath + fxExplosionFarPath);
	fxMegaphoneBuffer.loadFromFile(basePath + fxMegaphonePath);
	fxBombDropBuffer.loadFromFile(basePath + fxBombDropPath);
	fxSpawnerReadyBuffer.loadFromFile(basePath + fxSpanwerReadyPath);

	fxSiren.setBuffer(fxAlarmBuffer);
	fxSiren.setLoop(true);
	fxMegaphone.setBuffer(fxMegaphoneBuffer);
	fxBombDrop.setBuffer(fxBombDropBuffer);
	fxSpawnerReady.setBuffer(fxSpawnerReadyBuffer);
}

AudioManager::~AudioManager()
{
}

void AudioManager::playSoundtrackMain()
{
	stopSoundtrack();
	if (!soundtrack.openFromFile(basePath + soundtrackMainPath[std::rand() % SOUNDTRACK_MAIN_NUMBER]))
		return;
	soundtrack.play();
}

void AudioManager::playSoundtrackMenu()
{
	stopSoundtrack();
	if (!soundtrack.openFromFile(basePath + soundtrackMenuPath))
		return;
	soundtrack.play();
}

void AudioManager::playFxSiren()
{
	if (fxSiren.getStatus() != sf::SoundSource::Playing) {
		fxSiren.play();
	}
}

void AudioManager::playFxMegaphone()
{
	if (fxMegaphone.getStatus() != sf::SoundSource::Playing) {
		fxMegaphone.play();
	}
}

void AudioManager::playFxExplosion(bool away)
{
	fxBomb.setBuffer(away?fxBombFarBuffer:fxBombBuffer);
	fxBomb.play();
}

void AudioManager::playBombDrop()
{
	if (fxBombDrop.getStatus() == sf::SoundSource::Playing) {
		fxBombDrop.stop();
	}
	fxBombDrop.play();
}

void AudioManager::playSpawnerReady()
{
	if (fxSpawnerReady.getStatus() == sf::SoundSource::Playing) {
		fxSpawnerReady.stop();
	}
	fxSpawnerReady.play();
}

void AudioManager::stopFxSiren()
{
	if(fxSiren.getStatus()==sf::SoundSource::Playing)
		fxSiren.stop();
}

void AudioManager::stopSoundtrack()
{
	if (soundtrack.getStatus() == sf::SoundSource::Playing)
		soundtrack.stop();
}
