#include "GameMenu.h"
#include "stdfx.h"
#include "global.h"


GameMenu::GameMenu()
{
	//imposto i colori iniziali dei pulsanti 
	red[0] = 51;
	red[1] = 51;
	red[2] = 51;

	green[0] = 153;
	green[1] = 153;
	green[2] = 153;

	blu[0] = 255;
	blu[1] = 255;
	blu[2] = 255;
}


GameMenu::~GameMenu()
{
}

void GameMenu::idle(){
	glutPostRedisplay();
	return;
}

void GameMenu::render(){//funzione che disegna il men� iniziale

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	double w = glutGet(GLUT_WINDOW_WIDTH);
	double h = glutGet(GLUT_WINDOW_HEIGHT);

	float offset = 25;
	float offsetlat = 300;

	//variabili per il posizionamento dei pulsanti
	double sx = 0 + offsetlat;
	double rx = w - offsetlat;
	double up2 = (8 * offset);
	double down2 = (10 * offset);
	double up3 = (5 * offset);
	double down3 = (7 * offset);
	double up4 = (2 * offset);
	double down4 =  (4 * offset);

	//immagine come sfondo
	glBegin(GL_QUADS);
	glTexCoord2i(0, 0); glVertex2i(0, 0);
	glTexCoord2i(0, 1); glVertex2i(0, h);
	glTexCoord2i(1, 1); glVertex2i(w, h);
	glTexCoord2i(1, 0); glVertex2i(w, 0);
	glEnd();


	glDisable(GL_TEXTURE_2D);

	glPushAttrib(GL_CURRENT_BIT); //i colori impostati di seguito non influiranno sullo sfondo

	//bottone 1 quello pi� in basso
	glColor3ub(66, 66, 66); //sfondo
	glBegin(GL_QUADS);
	glVertex2f(sx + 2, up2 + 2);
	glVertex2f(sx + 2, down2 - 3);
	glVertex2f(rx - 3, down2 - 3);
	glVertex2f(rx - 3, up2 + 2);
	glEnd();

	glColor3ub(red[0], green[0], blu[0]);//cornice
	glBegin(GL_LINE_LOOP);
	glVertex2f(sx, up2);
	glVertex2f(sx, down2);
	glVertex2f(rx, down2);
	glVertex2f(rx, up2);
	glEnd();
	output((w / 2) - 70, up2 + offset + 5, "          QUIT");//scritta


	//bottone 2
	glColor3ub(66, 66, 66);
	glBegin(GL_QUADS);
	glVertex2f(sx + 2, up3 + 2);
	glVertex2f(sx + 2, down3 - 3);
	glVertex2f(rx - 3, down3 - 3);
	glVertex2f(rx - 3, up3 + 2);
	glEnd();
	glColor3ub(red[1], green[1], blu[1]);
	glBegin(GL_LINE_LOOP);
	glVertex2f(sx, up3);
	glVertex2f(sx, down3);
	glVertex2f(rx, down3);
	glVertex2f(rx, up3);
	glEnd();
	output((w / 2) - 60, up3 + offset + 5, "HOW TO PLAY");

	//bottone 3 quello pi� in alto
	glColor3ub(66, 66, 66);
	glBegin(GL_QUADS);
	glVertex2f(sx + 2, up4 + 2);
	glVertex2f(sx + 2, down4 - 3);
	glVertex2f(rx - 3, down4 - 3);
	glVertex2f(rx - 3, up4 + 2);
	glEnd();
	glColor3ub(red[2], green[2], blu[2]);
	glBegin(GL_LINE_LOOP);
	glVertex2f(sx, up4);
	glVertex2f(sx, down4);
	glVertex2f(rx, down4);
	glVertex2f(rx, up4);
	glEnd();
	output((w / 2) - 57, up4 + offset + 5, "START GAME");

	if (istruzioni){//disegno nel caso in cui ci si trovi dentro il sottomen� come how to play

		glColor3ub(66,66,66);//sfondo
		glBegin(GL_QUADS);
		glVertex2f((w / 10), (h / 10));
		glVertex2f((w / 10), h - (h / 10));
		glVertex2f(w - (w / 10), h - (h / 10));
		glVertex2f(w - (w / 10), (h / 10));
		glEnd();

		glColor3ub(55,153,255);//scritte
		output(w / 8, h / 6, "Player 1 is Coast Guard, player 2 is Boat");
		output(w / 8, h / 6 + 50, "P1 --- Target ----> Catch P2 going near to it and avoiding bombs");
		output(w / 8, h / 6 + 100, "P1 --------> Use A (left) D (right) to move, your boat is faster than P2");
		output(w / 8, h / 6 + 200, "P2 --- Target ----> Sink down P1 before getting caught");
		output(w / 8, h / 6 + 250, "P2 -------> Use arrow keys to move left and right <- ->");
		output(w / 8, h / 6 + 300, "P2 -------> Use '-' button to drop a bomb");
		output(w / 8, h / 6 + 450, "Press SPACE to skip intro");

		output(w / 2 - 210, h - h / 8, "PRESS ENTER TO RETURN TO THE MAIN SCREEN");

	}
	glPopAttrib();

	glEnable(GL_TEXTURE_2D);

	glFlush();
	glutSwapBuffers();

}

void GameMenu::reshape(int w, int h){

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, w, h, 0);
	glMatrixMode(GL_MODELVIEW);
	glViewport(0, 0, w, h);

}

void GameMenu::keyboard(unsigned char key, int x, int y){
	if (!istruzioni){//caso in cui ti trovi nel men� iniziale e non nel men� how to play
		switch (key) {
		case 27:
			exit(1);
			break;
		case 's'://scende
			switch (pos){
			case 0:
				accendiIniziaPartita();
				pos = 1;
				glutPostRedisplay();
				break;
			case 1:
				accendiOpzioni();
				pos = 2;
				glutPostRedisplay();
				break;
			case 2:
				accendiEsci();
				pos = 3;
				glutPostRedisplay();
				break;
			case 3:
				accendiIniziaPartita();
				pos = 1;
				glutPostRedisplay();
				break;
			}
			break;
		case 'w'://sale
			switch (pos){
			case 0:
				accendiEsci();
				pos = 3;
				glutPostRedisplay();
				break;
			case 1:
				accendiEsci();
				pos = 3;
				glutPostRedisplay();
				break;
			case 2:
				accendiIniziaPartita();
				pos = 1;
				glutPostRedisplay();
				break;
			case 3:
				accendiOpzioni();
				pos = 2;
				glutPostRedisplay();
				break;
			}
			break;
		case 13://premo invio
			switch (pos){
			case 0: //se sei in posizione 0 (ovvero nessun bottone evidenziato) non fa nulla
				break;
			case 1:
				//INIZIA GIOCO entra nel vero e proprio gioco
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				glEnable(GL_LIGHTING);
				loadasset(mainScenePath);//carica la scena
				if (!InitGL())
				{
					fprintf(stderr, "Initialization failed");
					return;
				}
				mainscene.skybox->Create();
				glutGet(GLUT_ELAPSED_TIME);
				game = &mainscene;
				game->reshape(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
				game->startgame();
				break;
			case 2:
				//entra nel men� how to play
				istruzioni = true;
				glutPostRedisplay();
				break;
			case 3:
				//esce
				exit(1);
				glutPostRedisplay();
				break;
			}
			break;

		default:
			break;
		}
	}
	else{
		switch (key){
		case 13://se sei nel men� how to play smette di disegnare questo tornando al men� principale
			istruzioni = false;
			glutPostRedisplay();
		}
	}
}

void GameMenu::keyboardup(unsigned char key, int x, int y){

}

void GameMenu::arrowKeys(int key, int x, int y){
	if (!istruzioni) {//caso in cui ti trovi nel men� iniziale e non nel men� how to play
		switch (key) {
		case GLUT_KEY_DOWN://scende
			switch (pos) {
			case 0:
				accendiIniziaPartita();
				pos = 1;
				glutPostRedisplay();
				break;
			case 1:
				accendiOpzioni();
				pos = 2;
				glutPostRedisplay();
				break;
			case 2:
				accendiEsci();
				pos = 3;
				glutPostRedisplay();
				break;
			case 3:
				accendiIniziaPartita();
				pos = 1;
				glutPostRedisplay();
				break;
			}
			break;
		case GLUT_KEY_UP://sale
			switch (pos) {
			case 0:
				accendiEsci();
				pos = 3;
				glutPostRedisplay();
				break;
			case 1:
				accendiEsci();
				pos = 3;
				glutPostRedisplay();
				break;
			case 2:
				accendiIniziaPartita();
				pos = 1;
				glutPostRedisplay();
				break;
			case 3:
				accendiOpzioni();
				pos = 2;
				glutPostRedisplay();
				break;
			}
			break;
		}
	}
}

void GameMenu::arrowKeysUp(int key, int x, int y)
{
}

void GameMenu::startgame() {
	accendiIniziaPartita();
	pos = 1;
	AM->playSoundtrackMenu();
}

void GameMenu::accendiIniziaPartita(){
	//imposti i colori per far si che sia evidenziato il bottone start game e diselezianati gli altri
	red[0] = 51;
	red[1] = 51;
	red[2] = 255;

	green[0] = 153;
	green[1] = 153;
	green[2] = 255;

	blu[0] = 255;
	blu[1] = 255;
	blu[2] = 255;
}
void GameMenu::accendiOpzioni(){
	//imposti i colori per far si che sia evidenziato il bottone how to play e diselezianati gli altri
	red[0] = 51;
	red[1] = 255;
	red[2] = 51;

	green[0] = 153;
	green[1] = 255;
	green[2] = 153;


	blu[0] = 255;
	blu[1] = 255;
	blu[2] = 255;
}
void GameMenu::accendiEsci(){
	//imposti i colori per far si che sia evidenziato il bottone esci e diselezianati gli altri
	red[0] = 255;
	red[1] = 51;
	red[2] = 51;

	green[0] = 255;
	green[1] = 153;
	green[2] = 153;

	blu[0] = 255;
	blu[1] = 255;
	blu[2] = 255;
}