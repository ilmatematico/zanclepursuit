#pragma once
#include "skybox.h"
#include "GamePhase.h"
#include <ResourceLoader.h>
#include <Ocean.h>
#include <Triton.h>
#include <list>
#include <SFML/System/Clock.hpp>
#include "AudioManager.h"

class GameScene;
static unsigned long startTime = 0, frameCount = 0;// The three main Triton objects you need:

#define BOMBS 20
#define NODO_BOAT totalnodes - 1
#define NODO_GUARDIA_COSTIERA totalnodes - 2
#define COLLISION_FAR_THRESHOLD 50.0
#define CATCH_THRESHOLD 1.0
#define GC_INTRO_X -100
#define START_GC_DISTANCE -50 //-25

class Bomb
{
public:
	Bomb(double x, double y, double z, GameScene* );
	Bomb(Triton::Vector3 spawnPosition, GameScene* );
	~Bomb();
	void detonate();
	Triton::Vector3 getPosition();
	std::pair<bool, bool> checkCollision(Triton::Vector3);
	Triton::Vector3 getDistance(Triton::Vector3 );
private:
	Triton::Impact *impact;
	Triton::DecalHandle decalHandle;
	Triton::Vector3 bombSpawnedPosition;
	GameScene* gameScene;
	void initialize(GameScene* );
};

class Spawner
{
public:
	Spawner();
	Spawner(double x, double y, double z, GameScene*);
	Spawner(Triton::Vector3 , GameScene*);
	Spawner(const Spawner& src);
	Spawner& operator=(const Spawner& src);
	~Spawner();

	void render();
	void bombFalling();
	bool spawn();
	void setPosition(Triton::Vector3);
	void setX(double x);
	Triton::Vector3 getPosition();
	void reset();
	int getBombsLeft();
private:
	Triton::Vector3 spawnerPosition;
	GameScene* gameScene;
	sf::Clock timer, fallT;
	const float SPAWN_RATE = 2.5f;
	const double SPAWN_HEIGTH = 5.0;
	bool enableRender = false;
	bool isFalling = true, isReady=false;
	int bombCounter = BOMBS;
};

class GameScene : public GamePhase
{
public:
	GameScene();
	~GameScene();

	virtual void render();
	virtual void reshape(int w, int h);
	virtual void keyboard(unsigned char key, int x, int y);
	virtual void keyboardup(unsigned char key, int x, int y);
	virtual void arrowKeys(int key, int x, int y);
	virtual void arrowKeysUp(int key, int x, int y);

	virtual void idle();
	virtual void startgame();

	Triton::ResourceLoader *resourceLoader;
	Triton::Environment *environment;
	Triton::Ocean *ocean;
	SkyBox* skybox;

	int bombDecalTextureID;
	double startTritonTime;
	float modelScale;
	
private:
	Triton::Vector3 gc, boat, cameraPosition, cameraTarget;
	double x_coord_gc, z_coord_gc, z_angle_gc, z_angle_boat, xangle, xrot, yrot,
		maxspeed, speed, seaLevel, zx, cv, ik, ol, x_coord_boat;
	bool left = false, right = false, up = false,
		down = false, space = false, y_up = false,
		y_down = false, gcIntro = true, gcGo = true,
		leftArrow = false, rightArrow = false, skipIntro = false;
	int ncollision;
	int timer, second;
	bool finished = false, playerGc = false, playerBoat = false;
	bool win = false;
	bool spawnBomb = false;
	int fuel, maxfuel, minfuel;
	double frametime;
	int endtype;
	bool disegnaPunti = false;
	int posizionePunti;
	Triton::WakeGenerator *gcWake, *boatWake;
	std::list<Bomb> bombList;
	bool checkCatch();
	double DrawTritonFrame();
	Spawner bombarolo;
	Triton::RotorWash *affondaWash;
	double washSpeed;
	float virataMultyplier = 0.4f;
	float gcSpeedMultyplier = 0.1f;
};
