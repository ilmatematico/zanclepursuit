This directory contains integration samples for the Triton Ocean SDK:

DirectX9Sample: Illustrates an infinite ocean with a reflected skybox using DirectX9.

DirectX11Sample: Illustrates an infinite ocean with a reflected skybox using DirectX11.

OpenGLSample: Illustrates an infinite ocean with a reflected skybox using OpenGL 2.0.

DirectX9PatchSample: Illustrates using Triton to shade a user-drawn grid as 3D water, using DirectX9.

DirectX11PatchSample: Illustrates using Triton to shade a user-drawn grid as 3D water, using DirectX11.

OpenGLPatchSample: Illustrates using Triton to shade a user-drawn grid as 3D water, using OpenGL 2.0.

LightspeedSample: A simple Game Framework-based integration for use with Gamebryo Lightspeed 3.2, showing Triton water integrated with terrain.

OgreSample: An integration with Ogre 1.8 rendering an infinite ocean.

OpenSceneGraphSample: Illustrates an infinite ocean integrated with OpenSceneGraph.

OpenSceneGraphReflection: Builds on the OpenSceneGraphSample by adding in a ship, which reflects in Triton's water in addition to the skybox. This shows how to combine planar reflection maps with environmental cube maps with Triton and OSG.

CSharpSample: Includes the necessary DLL's to call Triton from a C# application, and a simple example implemented using XNA Game Studio 4.0.
